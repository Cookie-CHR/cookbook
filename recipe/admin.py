from django.contrib import admin
from recipe.models import *


# Register your models to admin site, then you can add, edit, delete and search your models in Django admin site.


@admin.register(RecipePost)
class RecipePostAdmin(admin.ModelAdmin):

    list_filter = ("title", "author")

    ordering = ("title", "author")


@admin.register(TagOf)
class TagOfAdmin(admin.ModelAdmin):
    list_display = ("post", "tagname")

    list_filter = ("post", "tagname")

    ordering = ("post", "tagname")


@admin.register(IngredientOf)
class IngredientOfAdmin(admin.ModelAdmin):
    list_display = ("recipe", "name", "quantity")

    list_filter = ("recipe", "name")

    ordering = ("recipe", "name")


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ("post", "author")

    list_filter = ("post", "author")

    ordering = ("post", "author")


