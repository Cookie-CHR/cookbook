from django.urls import path
from . import views

app_name = 'recipe'

urlpatterns = [
    path('<int:pk>/', views.RecipeDetail.as_view(), name='recipe_detail'),
    path('', views.RecipeList.as_view(), name='recipe_list'),

    path('create/form_1', views.recipe_create_1, name='recipe_create_1'),
    path('create/form_2', views.recipe_create_2, name='recipe_create_2'),
    path('create/form_3', views.recipe_create_3, name='recipe_create_3'),
    path('create/form_4', views.recipe_create_4, name='recipe_create_4'),
    path('create/form_5', views.recipe_create_5, name='recipe_create_5'),

    path('edit/<int:pk>', views.RecipeDetailEdit.as_view(), name='recipe_update'),
    path('delete/<int:pk>', views.RecipeDelete.as_view(), name='recipe_delete'),

    path('title_update/<int:pk>', views.TitleUpdate.as_view(), name='title_update'),
    path('content_update/<int:pk>', views.ContentUpdate.as_view(), name='content_update'),
    path('image_update/<int:pk>', views.ImageUpdate.as_view(), name='image_update'),

    path('ingredients_update/<int:pk>', views.IngredientUpdate.as_view(), name='ingredient_update'),
    path('ingredients_update_empty/<int:pk>', views.IngredientUpdateWithExtra.as_view(), name='ingredient_update_empty'),

    path('tag_update/<int:pk>', views.TagUpdate.as_view(), name='tag_update'),
    path('tag_update_empty/<int:pk>', views.TagUpdateWithExtra.as_view(), name='tag_update_empty'),

    path('comment_update/<int:pk>', views.CommentUpdate.as_view(), name='comment_update'),
    path('comment_delete/<int:pk>', views.CommentDelete.as_view(), name='comment_delete'),



]
