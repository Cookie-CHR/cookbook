from django import forms
from django.forms import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, ButtonHolder, Submit

from recipe.models import *


TagsChoices = [
    ("hot_dish",      "Hot dish"),
    ("cold_dish",     "Cold dish"),
    ("appetizer",     "Appetizer"),
    ("first_course",  "First course"),
    ("second_course", "Second course"),
    ("dessert",       "Dessert"),
    ("vegetarian",    "Vegetarian"),
    ("vegan",         "Vegan"),
    ("light",         "Light"),
    ("lactose_free",  "Lactose free"),
    ("gluten_free",   "Gluten free"),
]

IngredientsDistinct = list(IngredientOf.objects.values_list('name', flat=True).distinct().order_by('name'))
IngredientChoices = [(a, a) for a in IngredientsDistinct]


OrderByChoices = [
    ("creation_date",   "Creation date"),
    ("title",           "Title"),
    ("ratings__average",          "Rating"),
]


class TitleForm(forms.ModelForm):
    class Meta:
        model = RecipePost
        fields = ['title']


class ContentForm(forms.ModelForm):
    class Meta:
        model = RecipePost
        fields = ['content']


class ImageForm(forms.ModelForm):
    class Meta:
        model = RecipePost
        fields = ['image']


class TagForm(forms.ModelForm):
    tagname = forms.CharField(label='Tags: ', widget=forms.Select(choices=TagsChoices))

    class Meta:
        model = TagOf
        fields = ['tagname']
        unique_together = ('tagname', 'post',)


TagFormSet = inlineformset_factory(parent_model=RecipePost, model=TagOf, form=TagForm, extra=1, can_delete=True)
TagFormSetWithoutExtras = inlineformset_factory(parent_model=RecipePost, model=TagOf, form=TagForm, extra=0, can_delete=True)


class IngredientForm(forms.ModelForm):
    class Meta:
        model = IngredientOf
        fields = ['name', 'quantity']

    def clean(self):
        """
        ensure that ingredient name is always lower case.
        """

        cleaned_data = self.cleaned_data

        if any(self.errors):
            return

        cleaned_data['name'] = cleaned_data['name'].lower()

        return cleaned_data



IngredientFormSet = inlineformset_factory(parent_model=RecipePost, model=IngredientOf, form=IngredientForm, extra=1, can_delete=True)
IngredientFormSetWithoutExtras = inlineformset_factory(parent_model=RecipePost, model=IngredientOf, form=IngredientForm, extra=0, can_delete=True)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']


class FilterForm(forms.Form):
    search = forms.CharField(required=False)
    filter_tags = forms.MultipleChoiceField(choices=TagsChoices, widget=forms.CheckboxSelectMultiple())
    filter_ingredients = forms.MultipleChoiceField(choices=IngredientChoices, widget=forms.CheckboxSelectMultiple())
    order_by = forms.CharField(label='Order by: ', widget=forms.Select(choices=OrderByChoices))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div('search'),
            Div(
                Div('filter_tags', css_class='col-sm-6'),
                Div('filter_ingredients', css_class='col-sm-6'),
                css_class='row'
            ),
            Div('order_by'),

            ButtonHolder(
                Submit('submit', 'Submit', css_class='button white')
            )
        )
