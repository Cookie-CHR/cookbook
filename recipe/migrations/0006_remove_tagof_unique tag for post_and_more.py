# Generated by Django 4.0.4 on 2022-05-25 12:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0005_tagof_unique tag for post'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='tagof',
            name='unique tag for post',
        ),
        migrations.AlterUniqueTogether(
            name='tagof',
            unique_together={('post', 'tagname')},
        ),
    ]
