from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from star_ratings.models import Rating
from user.models import UserProfile


# Create your models here.


class RecipePost(models.Model):
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='blog_posts')
    title = models.CharField(max_length=255)
    content = models.TextField(default="")
    image = models.ImageField(upload_to='static/images', blank=True, null=True)
    creation_date = models.DateField(auto_now_add=True)
    ratings = GenericRelation(Rating, related_query_name='rating')

    def __str__(self):
        return f'{self.pk} - {self.title}'


class TagOf(models.Model):
    post = models.ForeignKey(RecipePost, on_delete=models.CASCADE)
    tagname = models.CharField(max_length=255, default="Debugging")

    def __str__(self):
        return f'{self.post} - {self.tagname}'

    class Meta:
        verbose_name_plural = "Tags of"
        unique_together = ('tagname', 'post')


class IngredientOf(models.Model):
    recipe = models.ForeignKey(RecipePost, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    quantity = models.CharField(max_length=255, default="q.b.")

    def __str__(self):
        return f'{self.recipe}: {self.name} {self.quantity}'

    class Meta:
        verbose_name_plural = "Ingredients of"
        unique_together = ('name', 'recipe')


class Comment(models.Model):
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    post = models.ForeignKey(RecipePost, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return f'{self.pk} - [{self.author}] in [{self.post}]'
