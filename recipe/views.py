from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from recipe.models import *
from recipe.forms import *
from django.contrib import messages
from django.views.generic.edit import FormMixin
from CookBook.recommends import recommended
from user.models import UserProfile


# Create your views here.


class RecipeDetail(FormMixin, DetailView):
    model = RecipePost
    template_name = "recipe_detail.html"
    form_class = CommentForm

    def get_success_url(self):
        object = self.get_object()
        return reverse_lazy('recipe:recipe_detail', kwargs={'pk': object.pk})

    def get_context_data(self, **kwargs):
        context = super(RecipeDetail, self).get_context_data(**kwargs)  # get the default context data
        context['ingredients'] = IngredientOf.objects.filter(recipe=self.get_object())
        context['tags'] = TagOf.objects.filter(post=self.get_object()).order_by('tagname')
        context['comments'] = Comment.objects.filter(post=self.get_object())
        context['form'] = CommentForm(initial={'post': self.object})

        context['recommended'] = recommended(self.get_object())
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        form.instance.author = UserProfile.objects.get(user=self.request.user.pk)
        form.instance.post = self.object

        if form.is_valid():
            messages.success(request, 'Commented successfully!')
            return self.form_valid(form)
        else:
            messages.error(request, form.errors.as_data())
            return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        form.save()
        return super(RecipeDetail, self).form_valid(form)


class RecipeDetailEdit(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = RecipePost
    template_name = "recipe_detail_edit.html"
    success_url = reverse_lazy('recipe:recipe_list')

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super(RecipeDetailEdit, self).get_context_data(**kwargs)  # get the default context data
        context['ingredients'] = IngredientOf.objects.filter(recipe=self.get_object())
        context['tags'] = TagOf.objects.filter(post=self.get_object()).order_by('tagname')
        return context


class RecipeList(ListView):
    model = RecipePost
    template_name = "recipe_list.html"

    def get_context_data(self, **kwargs):
        context = super(RecipeList, self).get_context_data(**kwargs)  # get the default context data
        context['newest'] = RecipePost.objects.order_by('-creation_date')[:4]
        context['rated'] = RecipePost.objects.order_by("-ratings__average")[:4]
        context['all'] = RecipePost.objects.order_by("-ratings__average")
        context['form'] = FilterForm(initial={
            'search': self.request.GET.get('search', ''),
            'filter_tags': self.request.GET.get('filter_tags', ''),
            'filter_ingredients': self.request.GET.get('filter_ingredients', ''),
            'order_by': self.request.GET.get('order_by', ''),
        })
        return context

    def get_queryset(self):
        query = self.request.GET.get('search')
        filter_tags = self.request.GET.get('filter_tags')
        filter_ingredients = self.request.GET.get('filter_ingredients')
        order_by = self.request.GET.get('order_by')

        results = RecipePost.objects.all()
        if query:
            results = results.filter(title__contains=query)
        if filter_tags:
            filtered_tags = TagOf.objects.filter(tagname=filter_tags)
            filtered_posts = filtered_tags.values_list('post', flat=True)
            results = results.filter(pk__in=filtered_posts)
        if filter_ingredients:
            filtered_ingredients = IngredientOf.objects.filter(name=filter_ingredients)
            filtered_posts = filtered_ingredients.values_list('recipe', flat=True)
            results = results.filter(pk__in=filtered_posts)
        if order_by:
            results = results.order_by(order_by)
            if order_by == "ratings__average" or order_by == "creation_date":
                results = results.order_by("-"+order_by)
        return results


def recipe_create_1(request):
    initial = {'title': request.session.get('title', None)}
    form = TitleForm(request.POST or None, initial=initial)
    if request.method == 'POST':
        if form.is_valid():
            request.session['title'] = form.cleaned_data['title']
            return HttpResponseRedirect(reverse_lazy('recipe:recipe_create_2'))
    return render(request, 'recipe_create_1.html', {'form': form})


def recipe_create_2(request):
    formset = IngredientFormSet(request.POST or None)
    if request.method == 'POST':
        if formset.is_valid():
            request.session['ingredients'] = []
            for f in formset:
                if f not in formset.deleted_forms:
                    try:
                        f_dict = {'name': f.cleaned_data['name'], 'quantity': f.cleaned_data['quantity']}
                        request.session['ingredients'].append(f_dict)
                    except KeyError:
                        messages.error(request, "Some ingredient fields are empty: if you want to delete them, check the 'Delete' sign")
                        return render(request, 'recipe_create_2.html', {'form': formset, messages: messages})
            return HttpResponseRedirect(reverse_lazy('recipe:recipe_create_3'))
        else:
            for message in formset.errors:
                messages.error(request, message.as_data())
    return render(request, 'recipe_create_2.html', {'form': formset, messages: messages})


def recipe_create_3(request):
    initial = {
        'content': request.session.get('content', None),
    }
    form = ContentForm(request.POST or None, initial=initial)
    if request.method == 'POST':
        if form.is_valid():
            request.session['content'] = form.cleaned_data['content']
            return HttpResponseRedirect(reverse_lazy('recipe:recipe_create_4'))
        else:
            messages.error(request, form.errors.as_data())
    return render(request, 'recipe_create_3.html', {'form': form})


def recipe_create_4(request):
    formset = TagFormSet(request.POST or None)
    if request.method == 'POST':
        if formset.is_valid():
            request.session['tags'] = []
            for f in formset:
                if f not in formset.deleted_forms:
                    f_dict = {'tagname': f.cleaned_data['tagname']}
                    request.session['tags'].append(f_dict)
            return HttpResponseRedirect(reverse_lazy('recipe:recipe_create_5'))
        else:
            for message in formset.errors:
                messages.error(request, message.as_data())
    return render(request, 'recipe_create_4.html', {'form': formset, messages: messages})


def recipe_create_5(request):
    form = ImageForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            recipe = form.save(commit=False)
            recipe.author = UserProfile.objects.get(user=request.user.pk)
            recipe.title = request.session['title']
            recipe.content = request.session['content']
            recipe.image = form.cleaned_data['image']
            recipe.save()

            for ingredient in request.session['ingredients']:
                IngredientOf.objects.create(name=ingredient.get("name"),
                                            quantity=ingredient.get("quantity"),
                                            recipe=recipe)
            for tag in request.session['tags']:
                TagOf.objects.create(tagname=tag.get("tagname"),
                                     post=recipe)

            messages.success(request, 'Recipe added successfully!')
            return HttpResponseRedirect(reverse_lazy('recipe:recipe_detail', kwargs={'pk': recipe.pk}))
        else:
            messages.error(request, form.errors.as_data())
    return render(request, 'recipe_create_5.html', {'form': form})


class RecipeDelete(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = RecipePost
    template_name = "recipe_delete.html"
    success_url = reverse_lazy('recipe:recipe_list')

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff


class CommentUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Comment
    form_class = CommentForm
    template_name = "comment_update.html"

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self):
        object = self.get_object()
        return reverse_lazy('recipe:recipe_detail', kwargs={'pk': object.post.pk})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()

        if form.is_valid():
            messages.success(request, 'Comment updated successfully!')
            return self.form_valid(form)
        else:
            messages.error(request, form.errors.as_data())
            return self.render_to_response(self.get_context_data(form=form))


class CommentDelete(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Comment
    template_name = "comment_delete.html"

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self):
        return reverse_lazy('recipe:recipe_detail', kwargs={'pk': self.object.post.pk})


class TitleUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = TitleForm
    template_name = 'title_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self, **kwargs):
        # obj = form.instance or self.object
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request, 'Title updated successfully!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        return super().form_invalid(form)


class ContentUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = ContentForm
    template_name = 'content_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self, **kwargs):
        # obj = form.instance or self.object
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.object.pk})

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.success(self.request, 'Content updated successfully!')
        return super().form_valid(form)


class ImageUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = ImageForm
    template_name = 'image_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self, **kwargs):
        # obj = form.instance or self.object
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.object.pk})

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.success(self.request, 'Image updated successfully!')
        return super().form_valid(form)


class IngredientUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = IngredientFormSetWithoutExtras
    template_name = 'ingredient_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super(IngredientUpdate, self).get_context_data(**kwargs)  # get the default context data
        recipe = RecipePost.objects.get(pk=self.kwargs['pk'])
        context['ingredients'] = IngredientOf.objects.filter(recipe=recipe)
        context['title'] = recipe.title
        context['pk'] = recipe.pk
        return context

    def get_success_url(self, **kwargs):
        # obj = form.instance or self.object
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request, 'ingredients updated successfully!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        super().form_invalid(form)


class IngredientUpdateWithExtra(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = IngredientFormSet
    template_name = 'ingredient_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super(IngredientUpdateWithExtra, self).get_context_data(**kwargs)  # get the default context data
        recipe = RecipePost.objects.get(pk=self.kwargs['pk'])
        context['ingredients'] = IngredientOf.objects.filter(recipe=recipe)
        context['title'] = recipe.title
        context['pk'] = recipe.pk
        return context

    def get_success_url(self, **kwargs):
        # obj = form.instance or self.object
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request, 'ingredients updated successfully!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        super().form_invalid(form)


class TagUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = TagFormSetWithoutExtras
    template_name = 'tag_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super(TagUpdate, self).get_context_data(**kwargs)  # get the default context datapost = TagOf.objects.get(pk=self.kwargs['pk']).post
        context['pk'] = self.kwargs['pk']
        return context

    def get_success_url(self, **kwargs):
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request, 'Tags updated successfully!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        super().form_invalid(form)


class TagUpdateWithExtra(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = RecipePost
    form_class = TagFormSet
    template_name = 'tag_update.html'

    def test_func(self):
        obj = self.get_object()
        return obj.author.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super(TagUpdateWithExtra, self).get_context_data(**kwargs)  # get the default context datapost = TagOf.objects.get(pk=self.kwargs['pk']).post
        context['pk'] = self.kwargs['pk']
        return context

    def get_success_url(self, **kwargs):
        return reverse_lazy('recipe:recipe_update', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request, 'tag added successfully!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors.as_data())
        super().form_invalid(form)

