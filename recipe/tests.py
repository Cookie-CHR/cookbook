from django.urls import reverse
from django.test import TestCase
from user.models import UserProfile
from django.contrib.auth.models import User
from recipe .models import RecipePost, IngredientOf, TagOf, Comment
from . import urls


def create_recipe(userprofile, title):
    return RecipePost.objects.create(author=userprofile, title=title)


def create_ingredient(recipe):
    name = "abc"+recipe.title
    return IngredientOf.objects.create(recipe=recipe, name=name)


def create_tag(recipe):
    return TagOf.objects.create(post=recipe, tagname="Debugging")


def create_comment(recipe, profile):
    text = recipe.title+" - "+profile.user.username
    return Comment.objects.create(post=recipe, author=profile, text=text)


class RecipeDetailViewTests(TestCase):
    def setUp(self):
        user = User.objects.create(username="user", password="cwutdg36t28!", email="user@gmail.com")
        profile = UserProfile.objects.create(user=user, description="description")

    def test_redirect_if_no_recipe(self):
        """
        No recipe --> page does not exist
        """
        random_pk = 1
        response = self.client.get(reverse('recipe:recipe_detail', args=[random_pk]))
        self.assertEqual(response.status_code, 404)

    def test_if_recipe_but_empty(self):
        """
        existing recipe -> page should exist, but nothing else should be here
        """
        recipe = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        response = self.client.get(reverse('recipe:recipe_detail', args=[recipe.pk]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.context['object'].title, 'Title of this recipe by user')
        self.assertQuerysetEqual(response.context['ingredients'], [])
        self.assertContains(response, "This recipe has no ingredients")
        self.assertQuerysetEqual(response.context['tags'], [])
        self.assertContains(response, "This recipe has no tags")
        self.assertQuerysetEqual(response.context['comments'], [])
        self.assertContains(response, "There are no comments in this recipe")
        self.assertQuerysetEqual(response.context['recommended'], [])

    def test_if_recipe_and_unrelated_stuff(self):
        """
        existing recipe -> page should exist, but nothing else should be displayed even if other stuff is present - aside from recommended
        """
        recipe = create_recipe(UserProfile.objects.get(pk=1), "recipe")
        another_recipe = create_recipe(UserProfile.objects.get(pk=1), "Another recipe")
        another_ingredient = create_ingredient(another_recipe)
        another_tag = create_tag(another_recipe)
        another_comment = create_comment(another_recipe, UserProfile.objects.get(pk=1))
        response = self.client.get(reverse('recipe:recipe_detail', args=[recipe.pk]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.context['object'].title, 'recipe')
        self.assertQuerysetEqual(response.context['ingredients'], [])
        self.assertContains(response, "This recipe has no ingredients")
        self.assertQuerysetEqual(response.context['tags'], [])
        self.assertContains(response, "This recipe has no tags")
        self.assertQuerysetEqual(response.context['comments'], [])
        self.assertContains(response, "There are no comments in this recipe")
        self.assertQuerysetEqual(response.context['recommended'], ['<RecipePost: 2 - Another recipe>'])

    def test_if_recipe_and_only_related_stuff(self):
        """
        existing recipe -> page should exist and be populated - aside from recommended
        """
        recipe = create_recipe(UserProfile.objects.get(pk=1), "recipe")
        ingredient = create_ingredient(recipe)
        tag = create_tag(recipe)
        another_comment = create_comment(recipe, UserProfile.objects.get(pk=1))
        response = self.client.get(reverse('recipe:recipe_detail', args=[recipe.pk]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.context['object'].title, 'recipe')
        self.assertQuerysetEqual(response.context['ingredients'], ['<IngredientOf: 1 - recipe: abcrecipe q.b.>'])
        self.assertQuerysetEqual(response.context['tags'], ['<TagOf: 1 - recipe - Debugging>'])
        self.assertQuerysetEqual(response.context['comments'], ['<Comment: 1 - [user] in [1 - recipe]>'])
        self.assertQuerysetEqual(response.context['recommended'], [])

    def test_if_two_complete_recipes(self):
        """
        page should exist, show its content and recommend other recipe
        """
        recipe = create_recipe(UserProfile.objects.get(pk=1), "recipe")
        another_recipe = create_recipe(UserProfile.objects.get(pk=1), "Another recipe")
        ingredient = create_ingredient(recipe)
        tag = create_tag(recipe)
        another_comment = create_comment(recipe, UserProfile.objects.get(pk=1))
        another_ingredient = create_ingredient(another_recipe)
        another_tag = create_tag(another_recipe)
        another_comment = create_comment(another_recipe, UserProfile.objects.get(pk=1))

        response = self.client.get(reverse('recipe:recipe_detail', args=[recipe.pk]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.context['object'].title, 'recipe')
        self.assertQuerysetEqual(response.context['ingredients'], ['<IngredientOf: 1 - recipe: abcrecipe q.b.>'])
        self.assertQuerysetEqual(response.context['tags'], ['<TagOf: 1 - recipe - Debugging>'])
        self.assertQuerysetEqual(response.context['comments'], ['<Comment: 1 - [user] in [1 - recipe]>'])
        self.assertQuerysetEqual(response.context['recommended'], ['<RecipePost: 2 - Another recipe>'])
