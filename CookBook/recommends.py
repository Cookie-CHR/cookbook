from recipe.models import RecipePost, TagOf, IngredientOf


def recommended(recipe):

    if not type(recipe) == RecipePost:
        #check if a recipe was passed
        return []

    recipe_tags = TagOf.objects.filter(post=recipe)
    recipe_ingredients = IngredientOf.objects.filter(recipe=recipe)

    #Create a dictionary: key will be the recipepost, value will be its rate
    rated_recipes = {}

    #Initialize dictionary (must contain all recipes, with an initial rating of 1)
    for r in RecipePost.objects.all():
        rated_recipes[r] = 1

    for tag in recipe_tags:
        tagsof_by_tag = TagOf.objects.filter(tagname=tag.tagname)
        for tagof in tagsof_by_tag:
            # Every recipe that used one of the tags has its score increased
            rated_recipes[tagof.post] += 1

    for ing in recipe_ingredients:
        ingsof_by_ing = IngredientOf.objects.filter(name__contains=ing.name)
        for ingof in ingsof_by_ing:
            # Every recipe that used one of the ingredients has its score increased
            rated_recipes[ingof.recipe] += 1

    # sort dictionary by value and get only the first 4 keys
    rated_recipes = sorted(rated_recipes.items(), key=lambda x:x[1], reverse=True)
    recipe_list = [i[0] for i in rated_recipes]
    recipe_list.remove(recipe)

    return recipe_list[:4]
