from django.views.generic import TemplateView
from recipe.models import RecipePost


class Maintenance(TemplateView):
    template_name = 'maintenance.html'


class Home(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)  # get the default context data
        context['newest'] = RecipePost.objects.order_by('-creation_date')[:4]
        context['rated'] = RecipePost.objects.order_by("-ratings__average")[:4]
        return context


