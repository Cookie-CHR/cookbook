from django.urls import reverse
from django.test import TestCase
from user.models import UserProfile
from django.contrib.auth.models import User
from recipe .models import RecipePost, IngredientOf, TagOf, Comment
from .recommends import recommended


def create_recipe(userprofile, title):
    return RecipePost.objects.create(author=userprofile, title=title)


def create_ingredient(recipe, name):
    return IngredientOf.objects.create(recipe=recipe, name=name)


def create_tag(recipe, tagname):
    return TagOf.objects.create(post=recipe, tagname=tagname)


class RecommendTests(TestCase):
    def setUp(self):
        user = User.objects.create(username="user", password="cwutdg36t28!", email="user@gmail.com")
        profile = UserProfile.objects.create(user=user, description="description")

    def test_if_NO_recipe(self):
        """
        Fuzz testing: If i call the function with no arguments, the function should raise an exception
        """

        self.assertRaises(TypeError, recommended)

    def test_if_two_recipes_passed(self):
        """
        Fuzz testing: if inconsistent data are passed, the function should return an empty list
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        str = "<1 - Title of this recipe by user>"

        self.assertQuerysetEqual(recommended([recipe1, recipe2]), [])
        self.assertQuerysetEqual(recommended(str), [])

    def test_if_only_one_recipe(self):
        """
        If there is only one recipe, the function should return an empty list
        (It should NOT return the same recipe!)
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")

        self.assertQuerysetEqual(recommended(recipe1), [])

    def test_if_two_recipes(self):
        """
        If there are two recipes, the second recipe should be evaluated by the process
        and, since there are no other similar recipes, it should be returned
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 2 - Title of this other recipe by user>'])

    def test_if_a_lot_of_recipes(self):
        """
        Only the 4 most similar recipes must be returned
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        for i in range(10):
            create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")

        self.assertEqual(len(recommended(recipe1)), 4)


    def test_if_similar_ingredient_affects_queryset(self):
        """
        If two recipes have one ingredient in common, they should be more similar
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        recipe3 = create_recipe(UserProfile.objects.get(pk=1), "Title of this third recipe by user")

        ingredient1 = create_ingredient(recipe1, "a")
        ingredient3 = create_ingredient(recipe3, "a")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 3 - Title of this third recipe by user>',
                                                        '<RecipePost: 2 - Title of this other recipe by user>'])

    def test_if_similar_tag_affects_queryset(self):
        """
        If two recipes have one tag in common, they should be more similar
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        recipe3 = create_recipe(UserProfile.objects.get(pk=1), "Title of this third recipe by user")

        tag1 = create_tag(recipe1, "appetizer")
        tag3 = create_tag(recipe3, "appetizer")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 3 - Title of this third recipe by user>',
                                                        '<RecipePost: 2 - Title of this other recipe by user>'])

    def test_if_uppercase_affects_ingredients(self):
        """
        Ingredients are preprocessed to be lowercase, so it should still count as similar
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        recipe3 = create_recipe(UserProfile.objects.get(pk=1), "Title of this third recipe by user")

        ingredient1 = create_ingredient(recipe1, "a")
        ingredient3 = create_ingredient(recipe3, "A")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 3 - Title of this third recipe by user>',
                                                        '<RecipePost: 2 - Title of this other recipe by user>'])

    def test_if_substring_ingredients(self):
        """
        A specification of the current ingredient (ex. "cabbage" and "black cabbage") should still be similar
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        recipe3 = create_recipe(UserProfile.objects.get(pk=1), "Title of this third recipe by user")

        ingredient1 = create_ingredient(recipe1, "a")
        ingredient3 = create_ingredient(recipe3, "b a")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 3 - Title of this third recipe by user>',
                                                        '<RecipePost: 2 - Title of this other recipe by user>'])

    def test_if_both_ingredients_and_tags(self):
        """
        If both ingredients and tags are present, recipe with most similarities is always first
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        recipe3 = create_recipe(UserProfile.objects.get(pk=1), "Title of this third recipe by user")
        recipe4 = create_recipe(UserProfile.objects.get(pk=1), "Title of this forth recipe by user")

        ingredient1 = create_ingredient(recipe1, "a")
        ingredient3 = create_ingredient(recipe3, "a")
        ingredient2 = create_ingredient(recipe2, "a")

        tag1 = create_tag(recipe1, "appetizer")
        tag3 = create_tag(recipe3, "appetizer")
        tag4 = create_tag(recipe4, "appetizer")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 3 - Title of this third recipe by user>',
                                                        '<RecipePost: 2 - Title of this other recipe by user>',
                                                        '<RecipePost: 4 - Title of this forth recipe by user>'])

    def test_if_different_data(self):
        """
        If the recipe has nothing in common with the others, all the recipes have same ranking
        """
        recipe1 = create_recipe(UserProfile.objects.get(pk=1), "Title of this recipe by user")
        recipe2 = create_recipe(UserProfile.objects.get(pk=1), "Title of this other recipe by user")
        recipe3 = create_recipe(UserProfile.objects.get(pk=1), "Title of this third recipe by user")
        recipe4 = create_recipe(UserProfile.objects.get(pk=1), "Title of this forth recipe by user")

        ingredient1 = create_ingredient(recipe1, "b")
        ingredient3 = create_ingredient(recipe3, "a")
        ingredient2 = create_ingredient(recipe3, "a")

        tag1 = create_tag(recipe1, "dessert")
        tag3 = create_tag(recipe3, "appetizer")
        tag4 = create_tag(recipe3, "appetizer")

        self.assertQuerysetEqual(recommended(recipe1), ['<RecipePost: 2 - Title of this other recipe by user>',
                                                        '<RecipePost: 3 - Title of this third recipe by user>',
                                                        '<RecipePost: 4 - Title of this forth recipe by user>'])
