from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView, DetailView, ListView, CreateView, UpdateView, DeleteView
from user.forms import UserCForm, UserUForm, UserProfileForm
from django.contrib.auth.models import User
from user.models import UserProfile
from recipe.models import RecipePost
from django.urls import reverse_lazy
from django.contrib import messages
from django.shortcuts import render, redirect


# Create your views here.


class UserDetail(DetailView):
    model = User
    template_name = "user_detail.html"

    def get_context_data(self, **kwargs):
        context = super(UserDetail, self).get_context_data(**kwargs)
        user = User.objects.get(pk=self.kwargs['pk'])
        context['detail_user'] = user
        context['user'] = self.request.user
        try:
            context['profile'] = UserProfile.objects.get(user=user.pk)
        except self.model.DoesNotExist:
            raise ObjectDoesNotExist('User Profile does not exist')
        context['recipes'] = RecipePost.objects.filter(author=UserProfile.objects.get(user=user.pk))
        return context


@method_decorator(staff_member_required, name='dispatch')
class UserList(LoginRequiredMixin, ListView):
    model = User
    template_name = "user_list.html"


class UserCreate(CreateView):
    model = User
    form_class = UserCForm
    template_name = "user_create.html"
    success_url = reverse_lazy('Home')


class UserUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = User
    template_name = "user_update.html"
    form_class = UserUForm

    def test_func(self):
        obj = self.get_object()
        return obj.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self):
        return reverse_lazy('user:user_profile', kwargs={'pk': self.request.user.id})


@method_decorator(staff_member_required, name='dispatch')
class UserDelete(LoginRequiredMixin, DeleteView):
    model = User
    template_name = "user_delete.html"
    success_url = reverse_lazy('user:user_list')


class UserProfileUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = UserProfile
    template_name = "profile_update.html"
    form_class = UserProfileForm

    def test_func(self):
        obj = self.get_object()
        return obj.user.pk == self.request.user.pk or self.request.user.is_staff

    def get_success_url(self):
        return reverse_lazy('user:user_profile', kwargs={'pk': self.request.user.id})
