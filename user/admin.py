from django.contrib import admin
from user.models import UserProfile


# Register your models to admin site, then you can add, edit, delete and search your models in Django admin site.


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):

    ordering = ("user", "description")
