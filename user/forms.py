from django import forms
from django.contrib.auth.models import User
from user.models import UserProfile
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from crispy_forms.helper import FormHelper


class UserCForm(UserCreationForm):
    helper = FormHelper()
    helper.form_id = 'user-crispy-form'
    helper.form_method = 'POST'

    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.save()
        UserProfile.objects.create(
            user=user, description=""
        )
        return user


class UserUForm(UserChangeForm):
    helper = FormHelper()
    helper.form_id = 'user-crispy-form'
    helper.form_method = 'POST'

    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password")

    def save(self, commit=True):
        user = super(UserUForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['description', 'picture']
