from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('profile/<int:pk>/', views.UserDetail.as_view(), name='user_profile'),
    path('list', views.UserList.as_view(), name='user_list'),
    path('signup', views.UserCreate.as_view(), name='signup'),
    path('user-edit/<int:pk>', views.UserUpdate.as_view(), name='user_update'),
    path('delete-profile/<int:pk>', views.UserDelete.as_view(), name='user_delete'),

    path('edit-profile/<int:pk>', views.UserProfileUpdate.as_view(), name='profile_update'),
]

