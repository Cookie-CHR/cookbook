from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.test import TestCase
from .models import UserProfile
from django.contrib.auth.models import User
from recipe .models import RecipePost
from . import urls


def create_user(username):
    email = username+"@gmail.com"
    return User.objects.create(username=username, password="cwutdg36t28!", email=email)


def create_profile(user):
    description = "This is the description of user "+user.username
    return UserProfile.objects.create(user=user, description=description)


def create_recipe(userprofile):
    title = "Title of this recipe by "+userprofile.user.username
    return RecipePost.objects.create(author=userprofile, title=title)


class UserDetailViewTests(TestCase):
    def test_redirect_if_no_user(self):
        """
        No user --> page does not exist
        """
        random_pk = 1
        response = self.client.get(reverse('user:user_profile', args=[random_pk]))
        self.assertEqual(response.status_code, 404)

    def test_if_user_but_no_profile(self):
        """
        No profile --> page does not exist (profile should be automatically created at signup)
        """
        user = create_user("user")
        self.assertRaises(TypeError, reverse('user:user_profile', args=[user.pk]))

    def test_if_user_and_profile(self):
        """
        existing profile and user -> page should exist
        Also, correct user and profile should be returned
        """
        user = create_user("user")
        profile = create_profile(user)
        response = self.client.get(reverse('user:user_profile', args=[user.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['detail_user'], user)
        self.assertEqual(response.context['profile'], profile)

    def test_recipes_if_no_recipes(self):
        """
        There should be no recipes by this user
        """
        user = create_user("user")
        profile = create_profile(user)

        response = self.client.get(reverse('user:user_profile', args=[user.pk]))

        self.assertContains(response, "There are no recipes by this user")

    def test_recipes_if_no_user_recipes(self):
        """
        There should be no recipes by this user
        """
        my_user = create_user("My_user")
        my_profile = create_profile(my_user)
        another_user = create_user("Another_user")
        another_profile = create_profile(another_user)
        another_recipe = create_recipe(another_profile)

        response = self.client.get(reverse('user:user_profile', args=[my_user.pk]))

        self.assertContains(response, "There are no recipes by this user")

    def test_recipes_if_only_user_recipes(self):
        """
        Recipes by this user should be displayed
        """
        my_user = create_user("My_user")
        my_profile = create_profile(my_user)
        my_recipe = create_recipe(my_profile)

        response = self.client.get(reverse('user:user_profile', args=[my_user.pk]))

        self.assertQuerysetEqual(response.context['recipes'], ['<RecipePost: 1 - Title of this recipe by My_user>'])

    def test_recipes_if_some_user_recipes(self):
        """
        Recipes by this user should be displayed
        """
        my_user = create_user("My_user")
        my_profile = create_profile(my_user)
        another_user = create_user("Another_user")
        another_profile = create_profile(another_user)
        my_recipe = create_recipe(my_profile)
        another_recipe = create_recipe(another_profile)

        response = self.client.get(reverse('user:user_profile', args=[my_user.pk]))

        self.assertQuerysetEqual(response.context['recipes'], ['<RecipePost: 1 - Title of this recipe by My_user>'])
