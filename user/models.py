from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="author_profile", on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)
    picture = models.ImageField(upload_to='static/images/users',
                                blank=True, null=True)

    def __str__(self):
        return f'{self.user}'
