# CookBook

Un sito web per la condivisione di ricette, utilizzabile da chiunque voglia ricercare o pubblicare una particolare ricetta online.


## Pacchetti utilizzati

Per il progetto sono stati utilizzati:  

• Django: il framework base.  
  https://www.djangoproject.com/  
• Sqlite: per la gestione del database.  
  https://sqlite.org/docs.html  
• Crispy-forms: applicazione del framework django per migliorare l’aspetto dei form.
  https://django-crispy-forms.readthedocs.io/en/latest/  
• Star-ratings: applicazione del framework django per gestire facilmente il rating.  
  https://pypi.org/project/django-star-ratings/  


## Installazione librerie

### Requisiti:

• qualsiasi distro linux;  
• cartella del progetto (se non se ne è già in possesso, clonarla con il comando ```git clone https://gitlab.com/Cookie-CHR/cookbook```);

### Procedimento:
Se non se ne è già in possesso, installare python e pip da riga di comando
   ```console
    sudo apt install python3.7
    sudo apt install python3-pip
   ```

Porsi nella cartella del progetto, e preferibilmente in una shell pipenv.  
Installare le librerie e i pacchetti necessari utilizzando l'apposito comandi da terminale
   ```console
    pip install -r requirements.txt
   ```